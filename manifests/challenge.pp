exec { "apt-update":
    command => "/usr/bin/apt-get update"   
}

package { ["tomcat7", "openjdk-7-jre", "nginx"]:
    ensure => installed,
    require => Exec["apt-update"]
}

service { "tomcat7":
    ensure => running,
    enable => true,
    hasstatus => true,
    hasrestart => true, 
    require => Package["tomcat7"]
}

service { "nginx":
    ensure => running,
    enable => true,
    hasstatus => true,
    hasrestart => true, 
    require => Package["nginx"]
}

file { "/var/lib/tomcat7/webapps/devopschallenge.war":
    source => "/vagrant/files/devopschallenge.war",
    owner => tomcat7,
    group => tomcat7,
    mode => 0644,
    require => Package["tomcat7"],
    notify => Service["tomcat7"]
}

file {"/etc/nginx/sites-available/":
    purge => true
}

file { "/etc/nginx/sites-available/challenge-app":
    source => "/vagrant/files/challenge-app",
    mode => 0644,
    require => Package["nginx"],
    notify => Service["nginx"]
}

file { "/etc/nginx/sites-enabled/challenge-app":
  ensure => "link",
  target => "/etc/nginx/sites-available/challenge-app"
}
