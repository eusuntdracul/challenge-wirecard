# Moip Configuration Management Challenge
Use this file to create your deployment documentation with the complete instructions.

To run this you should have installed Vagrant in your machine and navigate to the root folder (challenge-wirecard) and run the following command:

#Vagrant up

The Vagrantfile will build the machine with the IP address 192.168.50.10.

There is a provision in the Vagrantfile which will run the puppet file automatically, providing all the software necessary and deploying the war file to Tomcat webapps directory, as replacing the default configuration file in "/etc/nginx/sites-available/default" of Nginx to a custom one that I made to redirect the port 80 to 8080. 

The puppet script inside "challenge-wirecard/manifests/challenge.pp" will install all the software and validate the installations.

Unfortunately I wastn't able to find out why the devopschallenge.war didn't become available in the web browser once I had configure the webserver right.

In Catalina.out the log shows that was an error deploying the war application, even when I used a FTP client to insert manually on in other machine or when I changed the Java Version (6, 7, 8).

Thanks and best Regards,
Guilherme Fernandes Faria.

Thank you,
The Wirecard Recruiting Team
